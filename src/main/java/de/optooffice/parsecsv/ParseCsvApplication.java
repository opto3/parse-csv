package de.optooffice.parsecsv;


import de.optooffice.parsecsv.bean.Article;
import de.optooffice.parsecsv.service.ParserService;

import java.util.List;

public class ParseCsvApplication {

	public static void main(String[] args) {

		final String SHOP_PATH = "opto-shop/artikelexport_vonShop.csv";
		final String HAENDLER_PATH = "haendler/43411_von huetter.csv";
		final String CATEGORY_PATH = "category/category.csv";
		final String RESULT = "shop_artikel";

		final
		ParserService parserService = new ParserService();

		List<Article> articles = parserService.setArticlesPriceBaseOfCategory(HAENDLER_PATH,
				SHOP_PATH, CATEGORY_PATH);
		parserService.createCsv(articles, RESULT);
	}

}
