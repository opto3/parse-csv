package de.optooffice.parsecsv.service;

import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import de.optooffice.parsecsv.bean.Article;
import de.optooffice.parsecsv.bean.ArticleHaendler;
import de.optooffice.parsecsv.bean.Category;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@AllArgsConstructor
@NoArgsConstructor
public class ParserService {

    Logger log = Logger.getLogger(ParserService.class.getName());
    // 1- Extract/Load content pf csv file using headers
    public List<Article> readCsv(String file) {
        List<Article> articles = new ArrayList<>();
        try {
            articles = new CsvToBeanBuilder(new FileReader(file)).withSeparator(';')
                    .withType(Article.class).build().parse();
        } catch (FileNotFoundException ex) {
            log.info("Exception while reading Article file: " + ex.getMessage());
        }
         return articles;
    }

    public List<Category> readCategoryCsv(String file) {
        List<Category> categories = new ArrayList<>();
        try {
            categories = new CsvToBeanBuilder(new FileReader(file)).withSeparator(';')
                    .withType(Article.class).build().parse();
        } catch (FileNotFoundException ex) {
            log.info("Exception while reading Category file: " + ex.getMessage());
        }
        return categories;
    }

    public List<ArticleHaendler> readHaendlerCsv(String file) {
        List<ArticleHaendler> articles = new ArrayList<>();
        try {
            articles = new CsvToBeanBuilder(new FileReader(file)).withSeparator(';')
                    .withType(ArticleHaendler.class).build().parse();
        } catch (FileNotFoundException ex) {
            log.info("Exception while reading Haendler file: " + ex.getMessage());
        }
        return articles;
    }

    // 3- Remove all rows where the EAN-Nummer is empty
    private List<Article> removeArticleWithEmptyEan(List<Article> articles) {
        articles.removeIf(at -> at.getP_ean() != 0);
        return articles;
    }

    // 2- Compare the description (Bezeichnung) of haendler file with our local file when match set the EAN-nummer with
    // EAN-nummer of haendler
    private List<Article> setEAN(String haendlerPath, String optoPath) {
        List<Article> articles = readCsv(optoPath);
        List<ArticleHaendler> haendlerArticles = readHaendlerCsv(haendlerPath);

        List<Article> articleList = haendlerArticles.stream()
                .filter(haendlerArticle -> haendlerArticle.getEan() % 1 != 0)
                .flatMap(haendlerArticle ->
                        articles.stream()
                                .filter(article -> haendlerArticle.getBezeichnung().equals(article.getP_name_de()))
                                .peek(article -> article.setP_ean(haendlerArticle.getEan()))
                ).collect(Collectors.toList());

        return removeArticleWithEmptyEan(articleList);
    }

    // 4- Compare the description (Bezeichnung) of haendler file with our local file when match set the EAN-nummer with EAN-nummer of haendler
    // Ek preis
    public List<Article> setArticlesPriceBaseOfCategory(String haendlerPath, String optoPath, String categoryPath) {
        List<Article> articles = setEAN(haendlerPath, optoPath);
        List<ArticleHaendler> haendlerArticles = readHaendlerCsv(haendlerPath);
        List<Category> categories =  readCategoryCsv(categoryPath);
        HashMap<Long, Double> eanPreis = new HashMap<>();

        // for each ean in article shop, check if that exist in haendler article list and then set pourcentage

        for (ArticleHaendler articleHaendler : haendlerArticles) {
            for (Category category : categories) {
                if (articleHaendler.getCat_l1().equals(category.getPath_a()) &&
                        articleHaendler.getCat_l2().equals(category.getPath_b()) &&
                        articleHaendler.getCat_l3().equals(category.getPath_c())) {
                    double preis = calculatePreis(category, articleHaendler);
                    eanPreis.put(articleHaendler.getEan(), preis);
                }
            }
        }

        return haendlerArticles.stream()
                .filter(haendlerArticle -> haendlerArticle.getEan() % 1 != 0)
                .flatMap(haendlerArticle ->
                        articles.stream()
                                .filter(article -> haendlerArticle.getBezeichnung().equals(article.getP_name_de()))
                                .peek(article -> article.setP_priceNoTax(eanPreis.get(haendlerArticle.getEan())))
                ).collect(Collectors.toList());

    }

    // 5- create csv file
    public void createCsv(List<Article> articles, String fileName) {

        try {
            Writer writer = new FileWriter(fileName);
            StatefulBeanToCsv beanToCsv = new StatefulBeanToCsvBuilder(writer).build();
            beanToCsv.write(articles);
            writer.close();
        } catch (IOException | CsvDataTypeMismatchException | CsvRequiredFieldEmptyException ex) {
            log.info("Exception while creating new file: " + ex.getMessage());
        }
    }

    private double calculatePreis(Category category, ArticleHaendler articleHaendler) {
        return articleHaendler.getEk_preis1() + ((articleHaendler.getEk_preis1()) * category.getPercentage())/100;
    }
}
