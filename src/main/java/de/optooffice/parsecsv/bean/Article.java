package de.optooffice.parsecsv.bean;

import com.opencsv.bean.CsvBindByName;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Article {

    @CsvBindByName(column = "XTSOL", required = true)
    private String xtsql;

    @CsvBindByName
    private long p_id;

    @CsvBindByName
    private String p_model;

    @CsvBindByName
    private double p_stock;

    @CsvBindByName
    private int p_sorting;

    @CsvBindByName
    private int p_startpage;

    @CsvBindByName
    private int p_startpage_sort;

    @CsvBindByName
    private int p_shipping;

    @CsvBindByName
    private String p_tpl;

    @CsvBindByName
    private String p_opttpl;

    @CsvBindByName
    private int p_manufacturer;

    @CsvBindByName
    private int p_fsk18;

    @CsvBindByName
    private double p_priceNoTax;

    @CsvBindByName(column = "p_priceNoTax.1")
    private String p_priceNoTaxOne;

    @CsvBindByName(column = "p_priceNoTax.2")
    private String p_priceNoTaxTwo;

    @CsvBindByName(column = "p_priceNoTax.3")
    private String p_priceNoTaxThree;

    @CsvBindByName
    private int p_tax;

    @CsvBindByName
    private int p_status;

    @CsvBindByName
    private double p_weight;

    @CsvBindByName
    private long p_ean;

    @CsvBindByName
    private String code_isbn;

    @CsvBindByName
    private String code_upc;

    @CsvBindByName
    private String code_mpn;

    @CsvBindByName
    private String code_jan;

    @CsvBindByName
    private String brand_name;

    @CsvBindByName
    private double p_disc;

    @CsvBindByName
    private String p_date_added;

    @CsvBindByName
    private String p_last_modified;

    @CsvBindByName
    private String p_date_available;

    @CsvBindByName
    private double p_ordered;

    @CsvBindByName
    private double nc_ultra_shipping_costs;

    @CsvBindByName
    private int gm_show_date_added;

    @CsvBindByName
    private int gm_show_price_offer;

    @CsvBindByName
    private int gm_show_weight;

    @CsvBindByName
    private int gm_show_qty_info;

    @CsvBindByName
    private int gm_price_status;

    @CsvBindByName
    private double gm_min_order;

    @CsvBindByName
    private double gm_graduated_qty;

    @CsvBindByName
    private String gm_options_template;

    @CsvBindByName
    private int p_vpe;

    @CsvBindByName
    private int p_vpe_status;

    @CsvBindByName
    private double p_vpe_value;

    @CsvBindByName(column = "p_image.1")
    private String p_imageOne;

    @CsvBindByName(column = "p_image.2")
    private String p_imageTwo;

    @CsvBindByName(column = "p_image.3")
    private String p_imageThree;

    @CsvBindByName
    private String p_image;

    // -------- EN ----------

    @CsvBindByName(column = "p_name.en")
    private String p_name;

    @CsvBindByName(column = "p_desc.en")
    private String p_desc;

    @CsvBindByName(column = "p_shortdesc.en")
    private String p_shortdesc;

    @CsvBindByName(column = "p_checkout_information.en")
    private String p_checkout_information;

    @CsvBindByName(column = "p_meta_title.en")
    private String p_meta_title;

    @CsvBindByName(column = "p_meta_desc.en")
    private String p_meta_desc;

    @CsvBindByName(column = "p_meta_key.en")
    private String p_meta_key;

    @CsvBindByName(column = "p_meta_desc.de")
    private String p_meta_desc_de;

    @CsvBindByName(column = "p_keywords.en")
    private String p_keywords;

    @CsvBindByName(column = "p_url.en")
    private String p_url;

    @CsvBindByName(column = "gm_url_keywords.en")
    private String gm_url_keywords;

    @CsvBindByName(column = "rewrite_url.en")
    private String rewrite_url;

    // -------- DE ----------
    @CsvBindByName(column = "p_name.de")
    private String p_name_de;

    @CsvBindByName(column = "p_desc.de")
    private String p_desc_de;

    @CsvBindByName(column = "p_shortdesc.de")
    private String p_shortdesc_de;

    @CsvBindByName(column = "p_checkout_information.de")
    private String p_checkout_information_de;

    @CsvBindByName(column = "p_meta_title.de")
    private String p_meta_title_de;

    @CsvBindByName(column = "p_meta_key.de")
    private String p_meta_key_de;

    @CsvBindByName(column = "p_keywords.de")
    private String p_keywords_de;

    @CsvBindByName(column = "p_url.de")
    private String p_url_de;

    @CsvBindByName(column = "gm_url_keywords.de")
    private String gm_url_keywords_de;

    @CsvBindByName(column = "rewrite_url.de")
    private String rewrite_url_de;

    @CsvBindByName(column = "p_cat.de")
    private String p_cat_de;

    @CsvBindByName(column = "p_cat.en")
    private String p_cat;

    @CsvBindByName
    private String google_export_availability;

    @CsvBindByName
    private String google_export_condition;

    @CsvBindByName
    private String google_category;

    @CsvBindByName(column = "p_img_alt_text.en")
    private String p_img_alt_text;

    @CsvBindByName(column = "p_img_alt_text.1.en")
    private String p_img_alt_text_one;

    @CsvBindByName(column = "p_img_alt_text.2.en")
    private String p_img_alt_text_two;

    @CsvBindByName(column = "p_img_alt_text.3.en")
    private String p_img_alt_text_three;

    @CsvBindByName(column = "p_img_alt_text.de")
    private String p_img_alt_text_de;

    @CsvBindByName(column = "p_img_alt_text.1.de")
    private String p_img_alt_text_one_de;

    @CsvBindByName(column = "p_img_alt_text.2.de")
    private String p_img_alt_text_two_de;

    @CsvBindByName(column = "p_img_alt_text.3.de")
    private String p_img_alt_text_three_de;

    @CsvBindByName(column = "p_group_permission.0")
    private int p_group_permission;

    @CsvBindByName(column = "p_group_permission.1")
    private int p_group_permission_one;

    @CsvBindByName(column = "p_group_permission.2")
    private int p_group_permission_two;

    @CsvBindByName(column = "p_group_permission.3")
    private int p_group_permission_three;

    @CsvBindByName
    private String specials_qty;

    @CsvBindByName
    private String specials_new_products_price;

    @CsvBindByName
    private String expires_date;

    @CsvBindByName
    private String specials_status;

    @CsvBindByName
    private Double gm_priority;

    @CsvBindByName
    private String gm_changefreq;

    @CsvBindByName
    private int gm_sitemap_entry;

    @CsvBindByName
    private String p_qty_unit_id;

    @CsvBindByName
    private int p_type;

    @CsvBindByName
    private String p_property_image_list_id;

    // @CsvRecurse
    // private Author author;

}

 /*class Author {
    @CsvBindByName(column = "author given name")
    private String givenName;

    @CsvBindByName(column = "author surname")
    private String surname;

    // Accessor methods go here.
}*/
