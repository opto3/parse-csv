package de.optooffice.parsecsv.bean;

import com.opencsv.bean.CsvBindByPosition;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Category {

    @CsvBindByPosition(position = 0)
    private String path_a;

    @CsvBindByPosition(position = 1)
    private String path_b;

    @CsvBindByPosition(position = 2)
    private String path_c;

    @CsvBindByPosition(position = 3)
    private String path_nb;

    // TODO: Check me (Type)
    @CsvBindByPosition(position = 4)
    private int percentage;

}
