package de.optooffice.parsecsv.bean;

import com.opencsv.bean.CsvBindByName;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ArticleHaendler {

    @CsvBindByName(column = "<#MenueBaumLevel1#>")
    private String cat_l1;

    @CsvBindByName(column = "<#MenueBaumLevel2#>")
    private String cat_l2;

    @CsvBindByName(column = "<#MenueBaumLevel3#>")
    private String cat_l3;

    @CsvBindByName(column = "<#Bezeichnung#>")
    private String bezeichnung;

    @CsvBindByName(column = "<#ArtikelNr#>")
    private String artikelNr;

    @CsvBindByName(column = "<#EAN#>")
    private long ean;

    @CsvBindByName(column = "<#Mwst19#>")
    private int mwst;

    @CsvBindByName(column = "<#Hersteller#>")
    private String hersteller;

    @CsvBindByName(column = "<#EK_Preis1#>")
    private double ek_preis1;

    @CsvBindByName(column = "<#EK_Menge2#>")
    private double menge2;

    @CsvBindByName(column = "<#EK_Preis2#>")
    private double ek_preis2;

    @CsvBindByName(column = "<#EK_Menge3#>")
    private double menge3;

    @CsvBindByName(column = "<#EK_Preis3#>")
    private double ek_preis3;

    @CsvBindByName(column = "<#EK_Menge4#>")
    private double menge4;

    @CsvBindByName(column = "<#EK_Preis4#>")
    private double ek_preis4;

    @CsvBindByName(column = "<#EK_Menge5#>")
    private double menge5;

    @CsvBindByName(column = "<#EK_Preis5#>")
    private double ek_preis5;

    @CsvBindByName(column = "<#Preis_VK1#>")
    private String preis_vk1;

    @CsvBindByName(column = "<#Preis_VK2#>")
    private String preis_vk2;

    @CsvBindByName(column = "<#Preis_VK3#>")
    private String preis_vk3;

    @CsvBindByName(column = "<#Mwst19#>")
    private int mwst19;

    @CsvBindByName(column = "<#UEVP#>")
    private double uevp;

    @CsvBindByName(column = "<#Lager_KZ#>")
    private int lager;

}
